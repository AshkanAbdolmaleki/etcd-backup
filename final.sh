#!/bin/bash

# set dates for backup rotation
NOWDATE=`date '+%Y-%m-%d_%H:%M:%S'`
BUCKET='etcd-backup-first'
DESTDIR='daily-backup'

set -ex

if [ ! -d /opt/kubernetes/backup ];
    then
        mkdir -p /opt/kubernetes/backup
fi

  kubectl exec -n kube-system \
    $(kubectl get pod -n kube-system \
    -o=jsonpath='{.items[*].metadata.name}' | \
    sed 's/[[:space:]]/\n/g' | \
    grep '^etcd') -- sh -c "ETCDCTL_API=3 \
    ETCDCTL_CACERT=/etc/kubernetes/pki/etcd/ca.crt \
    ETCDCTL_CERT=/etc/kubernetes/pki/etcd/server.crt \
    ETCDCTL_KEY=/etc/kubernetes/pki/etcd/server.key \
    etcdctl --endpoints=https://127.0.0.1:2379 \
    snapshot save /var/lib/etcd/snapshot.db"

    if [ -f /var/lib/etcd/snapshot.db ];
        then
            cp /var/lib/etcd/snapshot.db /opt/kubernetes/snapshot.db-$NOWDATE
            cp -r /etc/kubernetes/pki/etcd /opt/kubernetes/etcd-$NOWDATE
    fi

  ls -lha /opt/kubernetes/

  gzip /opt/kubernetes/snapshot.db-$NOWDATE
  tar -zcvf /opt/kubernetes/etcd-$NOWDATE.gz /opt/kubernetes/etcd-$NOWDATE

  # send the file off to s3
 s3cmd put /opt/kubernetes/etcd-$NOWDATE.gz s3://$BUCKET/$DESTDIR/
 s3cmd put /opt/kubernetes/snapshot.db-$NOWDATE.gz s3://$BUCKET/$DESTDIR/

 rm -rf /opt/kubernetes/*
